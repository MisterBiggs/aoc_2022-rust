use itertools::Itertools;
use std::fs;

pub fn run() {
    println!("Day 1:");
    let input = fs::read_to_string("./inputs/day1.txt").expect("Could not read file");

    println!("\tPart 1: {}", part1(&input));
    println!("\tPart 2: {}", part2(&input));
}

fn part1(food_input: &str) -> usize {
    food_input
        .split("\n\n")
        .into_iter()
        .map(|elf| {
            elf.split_whitespace()
                .map(|food| food.parse::<usize>().unwrap())
                .sum()
        })
        .into_iter()
        .max()
        .unwrap()
}
fn part2(food_input: &str) -> usize {
    food_input
        .split("\n\n")
        .collect::<Vec<&str>>()
        .into_iter()
        .map(|elf| {
            elf.split_whitespace()
                .map(|food| food.parse::<usize>().unwrap())
                .sum::<usize>()
        })
        .sorted()
        .rev()
        .take(3)
        .sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1() {
        let input = "1000
2000
3000

4000

5000
6000

7000
8000
9000

10000";

        assert_eq!(part1(input), 24000);
    }

    #[test]
    fn test_2() {
        let input = "1000
2000
3000

4000

5000
6000

7000
8000
9000

10000";

        assert_eq!(part2(input), 45000);
    }
}
