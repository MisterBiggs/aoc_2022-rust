use itertools::Itertools;
use std::collections::HashMap;
use std::collections::HashSet;
use std::fs;

pub fn run() {
    println!("Day 3:");
    let input = fs::read_to_string("./inputs/day3.txt").expect("Could not read file");

    println!("\tPart 1: {}", part1(&input));
    println!("\tPart 2: {}", part2(&input));
}

fn part1(input: &str) -> usize {
    let mut alphabet_map = HashMap::new();
    let alph = "abcdefghijklmnopqrstuvwxyz";
    for (value, letter) in format!("{}{}", alph, alph.to_uppercase())
        .chars()
        .enumerate()
    {
        alphabet_map.insert(letter, value + 1);
    }

    input
        .split_whitespace()
        .map(|line| {
            let (left, right) = line.split_at(line.len() / 2);
            assert!(left.len() == right.len());

            let bag_intersection = left
                .chars()
                .collect::<HashSet<char>>()
                .intersection(&right.chars().collect::<HashSet<char>>())
                .collect::<HashSet<&char>>()
                .into_iter()
                .copied()
                .collect::<Vec<char>>();

            assert!(bag_intersection.len() == 1);

            alphabet_map.get(&bag_intersection[0]).unwrap()
        })
        .sum()
}

fn part2(input: &str) -> usize {
    let mut alphabet_map = HashMap::new();
    let alph = "abcdefghijklmnopqrstuvwxyz";
    for (value, letter) in format!("{}{}", alph, alph.to_uppercase())
        .chars()
        .enumerate()
    {
        alphabet_map.insert(letter, value + 1);
    }

    input
        .split_whitespace()
        .collect::<Vec<&str>>()
        .into_iter()
        .tuples()
        .map(|line| {
            let (left, mid, right) = line;

            let bag_intersection = left
                .chars()
                .collect::<HashSet<char>>()
                .intersection(&right.chars().collect::<HashSet<char>>())
                .collect::<HashSet<&char>>()
                .into_iter()
                .copied()
                .collect::<HashSet<char>>()
                .intersection(&mid.chars().collect::<HashSet<char>>())
                .collect::<HashSet<&char>>()
                .into_iter()
                .copied()
                .collect::<Vec<char>>();

            assert!(bag_intersection.len() == 1);

            alphabet_map.get(&bag_intersection[0]).unwrap()
        })
        .sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1() {
        let input = "vJrwpWtwJgWrhcsFMMfFFhFp
jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
PmmdzqPrVvPwwTWBwg
wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
ttgJtRGJQctTZtZT
CrZsJsPPZsGzwwsLwLmpwMDw";

        assert_eq!(part1(input), 157);
    }

    #[test]
    fn test_2() {
        let input = "vJrwpWtwJgWrhcsFMMfFFhFp
jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
PmmdzqPrVvPwwTWBwg
wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
ttgJtRGJQctTZtZT
CrZsJsPPZsGzwwsLwLmpwMDw";

        assert_eq!(part2(input), 70);
    }
}
