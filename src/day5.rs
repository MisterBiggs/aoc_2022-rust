use std::collections::{HashMap, VecDeque};
use std::fmt::format;
use std::fs;

use itertools::Itertools;

pub fn run() {
    println!("Day 5:");
    let input = fs::read_to_string("./inputs/day5.txt").expect("Could not read file");

    println!("\tPart 1: {}", part1(&input));
    println!("\tPart 2: {}", part2(&input));
}

#[derive(Debug)]
struct Command {
    amount: usize,
    from: usize,
    to: usize,
}

fn part1(input: &str) -> String {
    let (stack_input, moves_input) = input.split_once("\n\n").unwrap();

    let mut containers: HashMap<usize, VecDeque<char>> = HashMap::new();
    for row in stack_input.lines() {
        for (column, (lbracket, container, _rbracket, _)) in
            format!("{} ", row).chars().tuples().enumerate()
        {
            if lbracket == '[' && !container.is_whitespace() {
                containers
                    .entry(column + 1)
                    .or_insert_with(VecDeque::new)
                    .push_back(container);
            }
        }
    }

    let commands = moves_input.lines().map(|line| {
        let (amount, from, to) =
            scan_fmt!(line, "move {} from {} to {}", usize, usize, usize).unwrap();
        Command { amount, from, to }
    });

    for command in commands {
        for _ in 0..command.amount {
            let container = containers
                .entry(command.from)
                .or_insert_with(VecDeque::new)
                .pop_front()
                .unwrap_or(' ');

            if container != ' ' {
                containers
                    .entry(command.to)
                    .or_insert_with(VecDeque::new)
                    .push_front(container)
            }
        }
    }

    let mut answer = "".to_string();
    for column in 1..=containers.len() {
        answer = format!(
            "{}{}",
            answer,
            containers.get(&column).unwrap().front().unwrap()
        );
    }
    answer
}

fn part2(input: &str) -> String {
    let (stack_input, moves_input) = input.split_once("\n\n").unwrap();

    let mut containers: HashMap<usize, VecDeque<char>> = HashMap::new();
    for row in stack_input.lines() {
        for (column, (lbracket, container, _rbracket, _)) in
            format!("{} ", row).chars().tuples().enumerate()
        {
            if lbracket == '[' && !container.is_whitespace() {
                containers
                    .entry(column + 1)
                    .or_insert_with(VecDeque::new)
                    .push_back(container);
            }
        }
    }

    let commands = moves_input.lines().map(|line| {
        let (amount, from, to) =
            scan_fmt!(line, "move {} from {} to {}", usize, usize, usize).unwrap();
        Command { amount, from, to }
    });

    let mut crane = VecDeque::new();

    for command in commands {
        for _ in 0..command.amount {
            let container = containers
                .entry(command.from)
                .or_insert_with(VecDeque::new)
                .pop_front()
                .unwrap_or(' ');

            crane.push_back(container);
        }
        for _ in 0..command.amount {
            let container = crane.pop_back().unwrap();
            if container != ' ' {
                containers
                    .entry(command.to)
                    .or_insert_with(VecDeque::new)
                    .push_front(container)
            }
        }
    }

    let mut answer = "".to_string();
    for column in 1..=containers.len() {
        answer = format!(
            "{}{}",
            answer,
            containers.get(&column).unwrap().front().unwrap()
        );
    }
    answer
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_1() {
        let input = "    [D]    
[N] [C]    
[Z] [M] [P]
 1   2   3 

move 1 from 2 to 1
move 3 from 1 to 3
move 2 from 2 to 1
move 1 from 1 to 2";

        assert_eq!(part1(input), "CMZ");
    }

    #[test]
    fn test_2() {
        let input = "    [D]    
[N] [C]    
[Z] [M] [P]
 1   2   3 

move 1 from 2 to 1
move 3 from 1 to 3
move 2 from 2 to 1
move 1 from 1 to 2";

        assert_eq!(part2(input), "MCD");
    }
}
